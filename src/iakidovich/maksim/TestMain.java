package iakidovich.maksim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class TestMain {
    
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Collections.addAll(list,"x", "pid");
        parse(list);
    }
    
    private static void parse(List<String> list) {
        ListIterator<String> iterator;
        for (iterator = list.listIterator(); iterator.hasNext();) {
            String current = iterator.next();
            if (current.startsWith("-")) {
                System.out.println("bla");
            } else {
                iterator.previous();
                break;
            }
        }
    }
    
    protected void doSomething() {
        System.out.println("I am protected");
    }
}
