package iakidovich.maksim.binary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

import iakidovich.maksim.TestMain;

public class BinaryOptions {

    public static void main(String[] args) {

        int[] testArray = {100, 156, 188, 198, 234, 511, 654, 655, 678, 789, 800, 866, 899, 999, 0, 0, 0, 0, 0};
        
        System.out.println(binarySearch(testArray,189));
        System.out.println(binaryDelete(testArray, 654));
        printArray(testArray);
        System.out.println(testArray.length);
        System.out.println(binaryAdd(testArray, 155));
        printArray(testArray);
        System.out.println(binaryDelete(testArray, 999));
        printArray(testArray);
    }

    private static void printArray(int[] array) {
        System.out.print("[ ");
        for (int anArray : array) {
            System.out.print(anArray + ", ");
        }
        System.out.println("]");
    }
    
    private static int binarySearch(int[] sourceArray, int searchedValue) {
        int highIndex = (int) Arrays.stream(sourceArray)
                                    .filter(x -> x > 0)
                                    .count();
        int lowIndex = 0;
        int middleIndex = 0;
        while (lowIndex < highIndex) {
            middleIndex = (lowIndex + highIndex) / 2;
            if (sourceArray[middleIndex] == searchedValue) {
                return middleIndex;
            }
            if (sourceArray[middleIndex] < searchedValue) {
                lowIndex = ++middleIndex;
            } else {
                highIndex = --middleIndex;
            }
        }
        return middleIndex;
    }

    private static boolean binaryAdd(int[] sourceArray, int value) {
        int zeroValueIndex = (int) Arrays.stream(sourceArray)
                                             .filter(x -> x > 0)
                                             .count();
        int indexForAddedValue = binarySearch(sourceArray, value);
        if (sourceArray[indexForAddedValue] < value) {
            if (sourceArray[indexForAddedValue + 1] < value) {
                return binaryAdd(sourceArray, value, indexForAddedValue + 2, zeroValueIndex);
            } else {
                return binaryAdd(sourceArray, value, indexForAddedValue + 1, zeroValueIndex);
            }
        } else {
            if (sourceArray[indexForAddedValue - 1] < value) {
                return binaryAdd(sourceArray, value, indexForAddedValue, zeroValueIndex);
            } else {
                return binaryAdd(sourceArray, value, indexForAddedValue - 1, zeroValueIndex);
            }
        }
    }

    private static boolean binaryAdd(int[] sourceArray, int value, int indexForValue, int high) {
        for (int i = high; i > indexForValue; i--) {
            sourceArray[i] = sourceArray[i - 1];
        }
        sourceArray[indexForValue] = value;
        return true;
    }

    private static boolean binaryDelete(int[] sourceArray, int value) {
        int deletedValueIndex = binarySearch(sourceArray, value);
        if (sourceArray[deletedValueIndex] != value) {
            return false;
        }
        int zeroValueIndex = (int) Arrays.stream(sourceArray)
                                .filter(x -> x > 0)
                                .count();
        for (int i = (deletedValueIndex + 1); i <= zeroValueIndex; i++) {
            sourceArray[i - 1] = sourceArray[i];
        }
        return true;
    }
}
