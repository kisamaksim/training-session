package iakidovich.maksim.binary;

public class BinaryComeBack {
    public static void main(String[] args) {
        int[] array = {1,2,45,67,89,123,456,678,890};
        System.out.println(binarySearch(array, 67));
        System.out.println(binarySearch(array, 123));
        System.out.println(binarySearch(array, 890));
        System.out.println(binarySearch(array, 1));
    }
    
    public static int binarySearch(int[] sourceArray, int value) {
        int upperBound = sourceArray.length - 1;
        int bottomBound = 0;
        int middleBound = 0;
        while (bottomBound <= upperBound) {
            middleBound = (bottomBound + upperBound) / 2;
            if (sourceArray[middleBound] == value) {
                return middleBound;
            }
            if (sourceArray[middleBound] > value) {
                upperBound = middleBound - 1;
            } else {
                bottomBound = middleBound + 1;
            }
        }
        return middleBound;
    }
}
