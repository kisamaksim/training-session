package iakidovich.maksim.binary;

public class RecursionBinarySearch {

    public static void main(String[] args) {
        int[] array = {1,2,3,6,7,8,189,200};
        System.out.println(binarySearch(array, 1,array.length,0));
        System.out.println(binarySearch(array, 200,array.length,0));
        System.out.println(binarySearch(array, 189,array.length,0));
        System.out.println(binarySearch(array, 7,array.length,0));
    }


    private static int binarySearch(int[] array, int value, int h, int l) {
        int middle = (h + l) / 2;
        if (array[middle] == value) {
            return middle;
        }
        if (array[middle] < value) {
            return binarySearch(array, value, h, middle);
        } else {
            return binarySearch(array, value, middle, l);
        }
    }
}
